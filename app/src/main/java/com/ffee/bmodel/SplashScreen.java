package com.ffee.bmodel;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

/**
 * Created by hjlee on 16. 4. 15.
 */
public class SplashScreen extends Activity {
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }
    /** Called when the activity is first created. */
    Thread splashTread;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        StartAnimations();
    }

    FullItemList mFullItemList;
    CollectionItemList mCollectionItemList;
    private void StartAnimations() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
        anim.reset();
        LinearLayout l=(LinearLayout) findViewById(R.id.lin_lay);
        l.clearAnimation();
        l.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim.reset();
        ImageView iv = (ImageView) findViewById(R.id.splash);
        iv.clearAnimation();
        iv.startAnimation(anim);

        splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    // Splash screen pause time
                    boolean tryOnce = true;
                    while (waited < 1500) {
                        sleep(50);
                        waited += 50;
                    }
                    mFullItemList = FullItemList.getInstance();
                    mCollectionItemList = CollectionItemList.getInstance();
                    ItemListManager.LoadURLData(getApplicationContext(), mFullItemList, MainActivity.DATAFILE, true);
                    ItemListManager.LoadURLData(getApplicationContext(), mCollectionItemList, CollectionActivity.COLLECTION_DATAFILE_NAME, false);
                    if (mFullItemList.size () != 0) {
                        checkLastID();
                    }
                    Intent intent = new Intent(SplashScreen.this,
                            MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                    SplashScreen.this.finish();
                } catch (InterruptedException e) {
                    // do nothing
                } finally {
                    SplashScreen.this.finish();
                }

            }
        };
        splashTread.start();

    }

    private void checkLastID() {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MainActivity.LASTID, Context.MODE_PRIVATE);
        int lastid = sharedPreferences.getInt(MainActivity.LASTID, 0);
        if (lastid == 0 || lastid > mFullItemList.size() ) {
            lastid = mFullItemList.size() - 16;
            if (lastid < 0) {
                lastid = 0;
            }

            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt(MainActivity.LASTID, lastid);
            editor.commit();
        }
    }
}
