package com.ffee.bmodel;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;
import android.support.v4.widget.SwipeRefreshLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    WebView webView;
    FullItemList mFullItemList;
    CollectionItemList mCollectionItemList;
    MenuItem mShowAD;

    static public final String DATAFILE = "urldata";
    static public final String LASTID = "lastid";
    static private final String IS_SHOWPRICE = "is_showprice";

    static private boolean bIsFirstResume = true;
    SharedPreferences sharedPreferences;
    SharedPreferences spIsShowPrice;

    static private final boolean IS_JAVASCRIPTINTERFACE_DEBUG = false;

    private SwipeRefreshLayout mSwipeRefresh;
    private String mBackendServerAddress = "http://localhost:8080";
    @Override
    protected void onResume() {
        super.onResume();
        webView.loadUrl("file:///android_asset/" + "bikini.html");
        if (bIsFirstResume) {
            bIsFirstResume = false;
        } else {
            Boolean currentShowPrice = spIsShowPrice.getBoolean(IS_SHOWPRICE, false);
            updateShowPrice(currentShowPrice);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFullItemList = FullItemList.getInstance();
        mCollectionItemList = CollectionItemList.getInstance();

        /* for screen swipe refresh function */
        mSwipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_wrapper);
        mSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                webView.loadUrl("javascript:prependPage()");
                mSwipeRefresh.setRefreshing(false);
            }
        });

        sharedPreferences = getApplicationContext().getSharedPreferences(LASTID,Context.MODE_PRIVATE);
        spIsShowPrice = getApplicationContext().getSharedPreferences(IS_SHOWPRICE,Context.MODE_PRIVATE);
        spIsShowPrice.edit().putBoolean(IS_SHOWPRICE, false).commit();

        webView = (WebView) findViewById(R.id.webView);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.addJavascriptInterface(new JavaScriptInterface(this), "Android");
        /* overscroll disabled */
        webView.setOverScrollMode(View.OVER_SCROLL_NEVER);

        /* To support CORS */
        webSettings.setAllowFileAccess(true);
        webSettings.setAllowContentAccess(true);
        webSettings.setAllowFileAccessFromFileURLs(true);
        webSettings.setAllowUniversalAccessFromFileURLs(true);

        webSettings.setDefaultTextEncodingName("UTF-8");

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                Log.i("WebView:" + consoleMessage.messageLevel() + "," + consoleMessage.sourceId(), consoleMessage.message() + '\n');
                return super.onConsoleMessage(consoleMessage);
            }
        });
        initFireBase();
    }

    @Override
    protected void onPause() {
        super.onPause();
        ItemListManager.SyncURLData(getApplicationContext(), mFullItemList, DATAFILE);
        ItemListManager.SyncURLData(getApplicationContext(), mCollectionItemList, CollectionActivity.COLLECTION_DATAFILE_NAME);
    }

    public class JavaScriptInterface {
        Context mContext;

        /** Instantiate the interface and set the context */
        JavaScriptInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public int init() {
            return sharedPreferences.getInt(LASTID, 0);
            //return mFullItemList.size();
        }
        @JavascriptInterface
        public int cacheSize() {
            return mFullItemList.size();
        }
        @JavascriptInterface
        public int getId(int index) {
            int id = index % mFullItemList.size();
            if (IS_JAVASCRIPTINTERFACE_DEBUG) Log.i("getId","index:"+index+",val:"+id+",ID:"+mFullItemList.get(id).getId());
            return mFullItemList.get(id).getId();
        }

        @JavascriptInterface
        public String getLinkUrl(int index) {
            int id = index % mFullItemList.size();
            if (IS_JAVASCRIPTINTERFACE_DEBUG) Log.i("getLinkUrl","id:"+id+",val:"+mFullItemList.get(id).getLinkUrl());
            return mFullItemList.get(id).getLinkUrl();
        }

        @JavascriptInterface
        public String getImageUrl(int index) {
            int id = index % mFullItemList.size();
            if (IS_JAVASCRIPTINTERFACE_DEBUG) Log.i("getImageUrl","id:"+id+",val:"+mFullItemList.get(id).getImageUrl());
            return mFullItemList.get(id).getImageUrl();
        }

        @JavascriptInterface
        public int getPrice(int index) {
            int id = index % mFullItemList.size();
            if (IS_JAVASCRIPTINTERFACE_DEBUG) Log.i("getPrice","id:"+id+",val:"+mFullItemList.get(id).getPrice());
            return mFullItemList.get(id).getPrice();
        }

        @JavascriptInterface
        public boolean addItemInfo(String id, String imageUrl, String linkUrl, String price, String status) {
            if (IS_JAVASCRIPTINTERFACE_DEBUG) Log.i("addItemInfo","id:"+id+",val:"+imageUrl+linkUrl);
            return mFullItemList.add(new ItemInfo(id, imageUrl, linkUrl, price, status));
        }

        @JavascriptInterface
        public int saveId(int id) {
            //Log.i("saveId", "id:" + id% mFullItemList.size());
            if (id < 0 ) {
                Log.w("saveId", "abnormal id:" + id + ",mFullItemList.size():" + mFullItemList.size());
                id = 1;
            } else {
                if (id > mFullItemList.size()) {
                    Log.w("saveId", "data is not yet commit to app mem - id:" + id + ",mFullItemList.size():" + mFullItemList.size());
                    id = mFullItemList.size();
                }
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt(LASTID, id);
                editor.commit();
            }
            return id;
        }

        @JavascriptInterface
        public void addBlockId(int index) {
            int id = index % mFullItemList.size();
            ItemInfo blockItem = mFullItemList.get(id);
            if(blockItem.getId() != id) {
                Log.e("JavaScriptInterface", "deference index : "+id +",id : "+blockItem.getId());
            }
            blockItem.setState(ItemInfo.STATE_BLOCKED);
        }

        @JavascriptInterface
        public boolean isBlockId(int index) {
            int id = index % mFullItemList.size();
            return mFullItemList.get(id).getState() == ItemInfo.STATE_BLOCKED;
        }

        @JavascriptInterface
        public void addCollectionId(int id) {
            Log.w("JavaScriptInterface", "addCollectionId(): index:" + id);
            int ret = mCollectionItemList.addItem(mFullItemList.get(id));
            if (ret == CollectionItemList.RET_SUCCESS) {
                String addCollectionListSuccessToastString = getString(R.string.cl_success_to_add)+" ("+mCollectionItemList.size()+"/"+mCollectionItemList.getCollectionListMaxCnt()+")";
                Toast.makeText(mContext, addCollectionListSuccessToastString, Toast.LENGTH_LONG).show();
                mFullItemList.get(id).setState(ItemInfo.STATE_COLLECTED);
            } else if (ret == CollectionItemList.RET_FAIL_FULL) {
                Toast.makeText(mContext, R.string.cl_fail_to_add_full, Toast.LENGTH_LONG).show();
            } else if (ret == CollectionItemList.RET_FAIL_DUP) {
                Toast.makeText(mContext, R.string.cl_fail_to_add_dup, Toast.LENGTH_LONG).show();
            }
        }

        @JavascriptInterface
        public void showMessage(String s) {
            Toast.makeText(mContext, s, Toast.LENGTH_LONG).show();
        }

        @JavascriptInterface
        public boolean isShowPrice() {
            return spIsShowPrice.getBoolean(IS_SHOWPRICE, false);
        }

        @JavascriptInterface
        public String getBackendInfo() {
            return mBackendServerAddress;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        /*
        MenuItem collection = menu.findItem(R.id.action_collection);
        collection.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent intent = new Intent(MainActivity.this, CollectionActivity.class);
                startActivity(intent);
                return true;
            }
        });
        MenuItem about = menu.findItem(R.id.action_about);
        collection.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Toast.makeText(getApplicationContext(),"version 1.0",Toast.LENGTH_LONG).show();
                return true;
            }
        });
        */
        //mShowAD = menu.findItem(R.id.action_show_ad);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_collection) {
            Intent intent = new Intent(MainActivity.this, CollectionActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_display_price) {
            Boolean currentShowPrice = spIsShowPrice.getBoolean(IS_SHOWPRICE, false);
            updateShowPrice(!currentShowPrice);
            return true;
        /*} else if (id == R.id.action_show_ad) {
            showInterstitial();
            return true;
        } else if (id == R.id.action_sign_in) {
            Toast.makeText(getApplicationContext(),R.string.about,Toast.LENGTH_LONG).show();
            return true;*/
        } else if (id == R.id.action_about) {
            Toast.makeText(getApplicationContext(),R.string.about,Toast.LENGTH_LONG).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateShowPrice(Boolean isShowPrice) {
        spIsShowPrice.edit().putBoolean(IS_SHOWPRICE, isShowPrice).commit();

        if (isShowPrice) {
            webView.loadUrl("javascript:showPrice()");
        } else {
            webView.loadUrl("javascript:hidePrice()");
        }
    }
    /*
    InterstitialAd mInterstitialAd = null;
    private InterstitialAd newInterstitialAd() {
        InterstitialAd interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(getString(R.string.interstitial_ad_unit_id));
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                mShowAD.setEnabled(true);
                Log.i("AD", "onAdLoaded");
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                mShowAD.setEnabled(true);
                Log.i("AD", "onAdFailedToLoad" + errorCode);
            }

            @Override
            public void onAdClosed() {
                mInterstitialAd = newInterstitialAd();
                loadInterstitial();
            }
        });
        return interstitialAd;
    }

    private void showInterstitial() {
        // Show the ad if it's ready. Otherwise toast and reload the ad.
        if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.i("AD", "Ad did not load");
            mInterstitialAd = newInterstitialAd();
            loadInterstitial();
        }
    }
    private void loadInterstitial() {
        if (mShowAD != null)
            mShowAD.setEnabled(false);

        AdRequest adRequest = new AdRequest.Builder()
                .setRequestAgent("bikini:andorid").build();
        mInterstitialAd.loadAd(adRequest);
    }
    */
    private void initFireBase() {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference dbRef = database.getReference();

        dbRef.child("server").addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String address = dataSnapshot.getValue(String.class);
                        if (address == null) {
                            Toast.makeText(getApplicationContext(), "could not get the server info" ,Toast.LENGTH_LONG).show();
                        } else {
                            mBackendServerAddress = address;
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w("firebase", "server:onCancelled", databaseError.toException());
                    }
                }
        );
    }
}
