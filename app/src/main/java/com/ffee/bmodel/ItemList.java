package com.ffee.bmodel;

import java.util.Iterator;

/**
 * Created by KimGunSoo on 2016-04-15.
 */
abstract public class ItemList implements Iterable<ItemInfo> {
    public abstract int size();
    public abstract boolean add(ItemInfo item);
    public abstract void add(int index, ItemInfo item);
    public abstract void clear();
    public abstract Iterator iterator();
}