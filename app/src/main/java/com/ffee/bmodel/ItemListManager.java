package com.ffee.bmodel;

import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.util.Log;
import android.content.Context;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * Created by KimGunSoo on 2016-04-14.
 */
public class ItemListManager {
    private final static String LOG_TAG = "ItemListManager";

    public static void LoadURLData(Context appCtx, ItemList itemList, String dataFile, boolean bHeal) {
        if (itemList.size() == 0) { //singleton object not init yet
            if (fileExists(appCtx, dataFile)) { //try to read urldata cache
                Log.i(LOG_TAG, "use data file");
                LoadURLDataFile(appCtx, itemList, dataFile, bHeal);
            }

            if (itemList.size() == 0 && bHeal) { //fail to read urldata cache
                Log.i(LOG_TAG,"use asset file");
                ReadAssetDataFile(appCtx, itemList, dataFile); //make cache from asset
            }
        }
    }
    public static void SyncURLData(Context appCtx, ItemList itemList, String dataFile) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(appCtx.openFileOutput(dataFile, Context.MODE_PRIVATE));
            for(ItemInfo item :itemList) {
                outputStreamWriter.write(item.getImageUrl() + " " + item.getLinkUrl() + " " + item.getPrice() + " " + item.getState() + " " + item.getId() + "\n");
            }
            outputStreamWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static boolean fileExists(Context appCtx, String fileName) {
        File file = appCtx.getFileStreamPath(fileName);
        return file != null && file.exists();
    }

    private static final int ASSET_ITEM_COUNT = 15;
    private static void LoadURLDataFile(Context appCtx, ItemList itemList, String dataFile, boolean checkData) {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(appCtx.openFileInput(dataFile)));
            String line = null;
            boolean healFromAsset = false;
            int prevId = -1;
            int currId;
            while ((line = in.readLine())!=null) {
                String[] imageAndLink = line.split(" ");
                //image+link+price+state+id
                if (imageAndLink.length != 5) {
                    Log.w(LOG_TAG, "LoadURLDataFile: parsing file error : " + imageAndLink[0]);
                    if (itemList.size() < ASSET_ITEM_COUNT) {
                        Log.w(LOG_TAG, "LoadURLDataFile: try to heal from asset");
                        healFromAsset = true;
                    }
                    break;
                }
                if (checkData) {
                    currId = Integer.parseInt(imageAndLink[4]);
                    if (prevId != currId - 1) {
                        Log.w(LOG_TAG, "LoadURLDataFile: detect broken data id : " + prevId + ", next id : " + currId);
                        break;
                    } else {
                        prevId = currId;
                    }
                }
                Log.i(LOG_TAG, "LoadURLDataFile: ItemInfo : " + imageAndLink[0] + ", id : " + imageAndLink[4]);
                itemList.add(Integer.parseInt(imageAndLink[4]), new ItemInfo(imageAndLink));
            }
            Log.i(LOG_TAG, "LoadURLDataFile: read item count : " + itemList.size());
            in.close();
            if (healFromAsset) {
                itemList.clear();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void ReadAssetDataFile(Context appCtx, ItemList itemList, String fileName) {
        AssetManager am = appCtx.getAssets();
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(am.open(fileName)));
            String line = null;

            while ((line = in.readLine())!=null) {
                String[] imageAndLink = line.split(" ");
                //image+link+price+state+id
                if (imageAndLink.length != 5) {
                    Log.w(LOG_TAG, "ReadAssetDataFile: parsing file error : " + imageAndLink);
                }
                //Log.i("ReadAssetDataFile", "ItemInfo : " + imageAndLink[0] + imageAndLink[1] + "\n" + imageAndLink[2] + imageAndLink[3] + imageAndLink[4]);
                Log.i(LOG_TAG, "ReadAssetDataFile: ItemInfo : " + imageAndLink[0]+ ", id : " + imageAndLink[4]);
                itemList.add(Integer.parseInt(imageAndLink[4]), new ItemInfo(imageAndLink));
            }
            Log.i(LOG_TAG, "ReadAssetDataFile: read item count : " + itemList.size());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}