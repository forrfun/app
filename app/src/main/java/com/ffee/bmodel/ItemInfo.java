package com.ffee.bmodel;

/**
 * Created by gg on 16. 2. 3.
 */
public class ItemInfo {

    /*
    public enum State {
        NONE(0), COLLECTED(1), BLOCKED(2);

        private int value;
        private State (int value) {
            this.value = value;
        }
        public int getValue() {
            return this.value;
        }
    };
    */
    public static int STATE_NONE = 0;
    public static int STATE_COLLECTED = 1;
    public static int STATE_BLOCKED = 2;

    private int mid;
    private String mImageUrl;
    private String mLinkUrl;
    private int mPrice;
    private int mState;

    public int getState() {
        return mState;
    }

    public void setState(int mState) {
        this.mState = mState;
    }

    public int getPrice() {
        return mPrice;
    }

    public void setPrice(int mPrice) {
        this.mPrice = mPrice;
    }

    public int getId() {
        return mid;
    }

    public void setId(int mid) {
        this.mid = mid;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.mImageUrl = imageUrl;
    }

    public String getLinkUrl() {
        return mLinkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.mLinkUrl = linkUrl;
    }

    public ItemInfo() {
        mImageUrl = null;
        mLinkUrl = null;
        mPrice = 0;
        mState = STATE_NONE;
    }

    public ItemInfo(String[] imageUrlandlinkUrlandPrice) {
        mImageUrl = imageUrlandlinkUrlandPrice[0];
        mLinkUrl = imageUrlandlinkUrlandPrice[1];
        mPrice = Integer.parseInt(imageUrlandlinkUrlandPrice[2]);
        mState = Integer.parseInt(imageUrlandlinkUrlandPrice[3]);
        mid = Integer.parseInt(imageUrlandlinkUrlandPrice[4]);
    }

    public ItemInfo(String id, String imageUrl, String linkUrl, String price, String status) {
        mImageUrl = imageUrl;
        mLinkUrl = linkUrl;
        mPrice = Integer.parseInt(price);
        mState = Integer.parseInt(status);
        mid = Integer.parseInt(id);
    }
}
