package com.ffee.bmodel;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import com.google.android.gms.ads.InterstitialAd;

public class CollectionActivity extends ActionBarActivity {
    WebView mCollectionWebView;
    CollectionItemList mCollectionItemList;
    SharedPreferences mCollectionSharedPreferences;
    SharedPreferences spIsShowPrice;

    static private final String LOG_TAG = "CollectionActivity";
    static private final String ANDROID_ASSET_PATH = "file:///android_asset/";
    static private final String COLLECTION_HTML_PATH = ANDROID_ASSET_PATH + "collection.html";
    static private final String COLLECTION_LASTID = "collectionlastid";
    static private final String JAVASCRIPT_INTERFACE_ID = "AndroidCollection";
    static private final String IS_SHOWPRICE = "is_showprice";
    static private final String COLLECTION_TITLE_PREFIX = "My Collection";
    static private boolean bIsFirstResume = true;

    static private final boolean WEBCLIENT_DEBUG = true;

    static public final String COLLECTION_DATAFILE_NAME = "collectiondata";

    @Override
    protected void onResume() {
        super.onResume();

        /* Set the Bar attribute for CollectionList Activity */
        initCollectionActivityBar();

        /* Load the embedded html file that draw the collection list item through web engine. */
        mCollectionWebView.loadUrl(COLLECTION_HTML_PATH);

        if (bIsFirstResume) {
            bIsFirstResume = false;
        } else {
            Boolean currentShowPrice = spIsShowPrice.getBoolean(IS_SHOWPRICE, false);
            updateShowPrice(currentShowPrice);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collection);

        initCollectionActivityBar();
        initCollectionItemList();
        initCollectionWebView();
    }

    private void initCollectionActivityBar() {
        android.support.v7.app.ActionBar bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorAccent)));
        bar.setTitle(getCollectionTitleString());
    }

    private String getCollectionTitleString() {
        mCollectionItemList = CollectionItemList.getInstance();
        return COLLECTION_TITLE_PREFIX + " ("+mCollectionItemList.size()+"/"+mCollectionItemList.getCollectionListMaxCnt()+")";
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    private void initCollectionWebView() {
        mCollectionWebView = (WebView) findViewById(R.id.collectionWebView);
        WebSettings webSettings = mCollectionWebView.getSettings();
        webSettings.setDefaultTextEncodingName("UTF-8");

        webSettings.setJavaScriptEnabled(true);
        mCollectionWebView.addJavascriptInterface(new JavaScriptInterface(this), JAVASCRIPT_INTERFACE_ID);

        mCollectionWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                if (WEBCLIENT_DEBUG) {
                    Log.i(LOG_TAG + ":" + consoleMessage.messageLevel() + "," + consoleMessage.sourceId(), consoleMessage.message() + '\n');
                }
                return super.onConsoleMessage(consoleMessage);
            }
        });
    }

    private void initCollectionItemList() {
        mCollectionItemList = CollectionItemList.getInstance();
        mCollectionSharedPreferences = getApplicationContext().getSharedPreferences(COLLECTION_LASTID, Context.MODE_PRIVATE);
        spIsShowPrice = getApplicationContext().getSharedPreferences(IS_SHOWPRICE,Context.MODE_PRIVATE);

        //ItemListManager.LoadURLData(getApplicationContext(), mCollectionItemList, COLLECTION_DATAFILE_NAME, false);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //ItemListManager.SyncURLData(getApplicationContext(), mCollectionItemList, COLLECTION_DATAFILE_NAME);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_collection, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            Toast.makeText(getApplicationContext(), R.string.about, Toast.LENGTH_LONG).show();
            return true;
        } else if (id == R.id.action_display_price) {
            Boolean currentShowPrice = spIsShowPrice.getBoolean(IS_SHOWPRICE, false);
            updateShowPrice(!currentShowPrice);
            return true;
        }/* else if (id == R.id.action_show_ad) {
            Intent intent = new Intent(CollectionActivity.this, adActivity.class);
            startActivity(intent);
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    private void updateShowPrice(Boolean isShowPrice) {
        spIsShowPrice.edit().putBoolean(IS_SHOWPRICE, isShowPrice).commit();

        if (isShowPrice) {
            mCollectionWebView.loadUrl("javascript:showPrice()");
        } else {
            mCollectionWebView.loadUrl("javascript:hidePrice()");
        }
    }

    public class JavaScriptInterface {
        Context mContext;

        /**
         * Instantiate the interface and set the context
         */
        JavaScriptInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public int getLastUniqueID() {
            return mCollectionSharedPreferences.getInt(COLLECTION_LASTID, 1);
        }

        @JavascriptInterface
        public String getImageUrl(int index) {
            //Log.i("getImageUrl","id:"+id+",val:"+mFullItemList.get(id%mFullItemList.size()).getmImageUrl());
            return mCollectionItemList.getItem(index % mCollectionItemList.size()).getImageUrl();
        }

        @JavascriptInterface
        public String getLinkUrlByUniqueID(int uniqueID) {
            //Log.i("getLinkUrlByUniqueID","id:"+id+",val:"+mFullItemList.get(id%mFullItemList.size()).getmLinkUrl());
            return mCollectionItemList.getItemByUniqueID(uniqueID).getLinkUrl();
        }

        @JavascriptInterface
        public int getPrice(int index) {
            //Log.i("getPrice","id:"+id+",val:"+mFullItemList.get(index % mCollectionItemList.size()).getPrice());
            return mCollectionItemList.getItem(index % mCollectionItemList.size()).getPrice();
        }


        @JavascriptInterface
        public int saveLastUniqueID(int uniqueID) {
            //Log.i("saveId", "id:" + id% mFullItemList.size());
            SharedPreferences.Editor editor = mCollectionSharedPreferences.edit();
            editor.putInt(COLLECTION_LASTID, uniqueID);
            editor.commit();
            return uniqueID;
        }

        @JavascriptInterface
        public int getUniqueID(int index) {
            return mCollectionItemList.getItem(index).getId();
        }

        @JavascriptInterface
        public int removeItemByUniqueID(int uniqueID) {
            ItemInfo item = mCollectionItemList.getItemByUniqueID(uniqueID);
            if (item != null) {
                if (mCollectionItemList.removeItem(item) == true) {
                    return 0;
                }
            }
            return -1;
        }

        @JavascriptInterface
        public int getItemCnt() {
            return mCollectionItemList.size();
        }

        @JavascriptInterface
        public void showOneImage(int id) {
            Log.i("showOneImage", "id:"+id);
            Intent intent = new Intent(CollectionActivity.this, showOneImageActivity.class);
            intent.putExtra("image_source", id);
            startActivity(intent);
        }

        @JavascriptInterface
        public boolean isShowPrice() {
            return spIsShowPrice.getBoolean(IS_SHOWPRICE, false);
        }
    }
}
