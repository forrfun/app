package com.ffee.bmodel;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by gg on 16. 3. 14.
 */
public class FullItemList extends ItemList {

    private ArrayList<ItemInfo> mFullItemList;
    private static FullItemList instance;

    private FullItemList() {
        this.mFullItemList = new ArrayList<ItemInfo>();
    }
    public static synchronized FullItemList getInstance() {
        if (instance == null) {
            instance = new FullItemList();
        }
        return instance;
    }

    public int size() {
        return mFullItemList.size();
    }

    public synchronized void add(int index, ItemInfo item) {
        mFullItemList.add(index, item);
    }

    public synchronized boolean add(ItemInfo item) {
        int index = item.getId();
        if (mFullItemList.size() > index)
            return false;
        try {
            mFullItemList.add(index, item);
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public ItemInfo get(int id) {
        return mFullItemList.get(id);
    }

    public void clear() {
        mFullItemList.clear();
    }

    public Iterator iterator() {
        return mFullItemList.iterator();
    }
}
