package com.ffee.bmodel;

import android.util.Log;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by KimGunSoo on 2016-03-07.
 */
public class CollectionItemList extends ItemList{
    private static CollectionItemList instance;
    private static ArrayList<ItemInfo> mItemArrayList = null;
    private static Integer collection_list_max_cnt;

    private static final Integer COLLECTION_LIST_DEFAULT_MAX_CNT = 16;
    private static final String LOG_TAG = "CollectionItemList";

    public static final int RET_SUCCESS = 0;
    public static final int RET_FAIL_FULL = -1;
    public static final int RET_FAIL_DUP = -2;

    private CollectionItemList() {
        mItemArrayList = new ArrayList<ItemInfo>();
        collection_list_max_cnt = COLLECTION_LIST_DEFAULT_MAX_CNT;
    }

    public static synchronized CollectionItemList getInstance() {
        if (instance == null) {
            instance = new CollectionItemList();
        }
        return instance;
    }

    public synchronized Integer addItem(ItemInfo item) {
        if (mItemArrayList.contains(item) == false) {
            if (mItemArrayList.size() >= collection_list_max_cnt) {
                Log.w(LOG_TAG, "addItem: Collection list is full.(max:"+collection_list_max_cnt+")");
                return RET_FAIL_FULL;
            }
            mItemArrayList.add(item);
            return RET_SUCCESS;
        }
        return RET_FAIL_DUP;
    }

    public synchronized ItemInfo getItem(Integer index) {
        return mItemArrayList.get(index);
    }

    public synchronized ItemInfo getItemByUniqueID(Integer uniqueID) {
        Log.w(LOG_TAG, "getItemByUniqueID(): ID:"+uniqueID);
        for (ItemInfo itemInfo: mItemArrayList) {
            if (itemInfo.getId() == uniqueID) {
                return itemInfo;
            }
        }
        return null;
    }

    public synchronized boolean removeItem(ItemInfo item) {
        return mItemArrayList.remove(item);
    }
    public synchronized boolean removeItem(int id) {
        for (ItemInfo itemInfo: mItemArrayList) {
            if (itemInfo.getId() == id) {
                return mItemArrayList.remove(itemInfo);
            }
        }
        return false;
    }

    public synchronized int size() {
        return mItemArrayList.size();
    }

    @Override
    public boolean add(ItemInfo item) {
        Integer ret = addItem(item);
        if (RET_SUCCESS == ret) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void add(int index, ItemInfo item) {
        addItem(item);
    }

    @Override
    public void clear() {
        mItemArrayList.clear();
    }

    @Override
    public Iterator iterator() {
        return mItemArrayList.iterator();
    }

    public synchronized boolean setCollectionListMaxCnt(Integer maxCnt) {
        if (maxCnt <= 0) {
            return false;
        }
        collection_list_max_cnt = maxCnt;
        return true;
    }

    public synchronized Integer getCollectionListMaxCnt() {
        return collection_list_max_cnt;
    }
}